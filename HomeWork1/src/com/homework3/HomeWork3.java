package com.homework3;

import java.util.Scanner;

public class HomeWork3 {
  private static final String CHANGE = "change";
  private static final String RESCHEDULE = "reschedule";
  private static boolean isContinue = true;
  private static String[][] schedule = {
    {"monday", "do something"},
    {"tuesday", "homework"},
    {"wednesday", "study"},
    {"thursday", "one more time"},
    {"friday", "party time"},
    {"saturday", "have a fun"},
    {"sunday", "take a break"}
  };

  public static void main(String[] args) {
    while (isContinue) {
      checkSchedule(schedule);
    }
  }

  private static String scanner() {
    Scanner scan = new Scanner(System.in);
    if (scan.hasNext()) {
      return scan.nextLine();
    } else {
      return scanner();
    }
  }

  private static String[] getArrWord() {
    String str = scanner();
    return str.split(" ");
  }

  private static void checkSchedule(String[][] arr) {
    System.out.printf("Please, input the day of the week: %n");
    String[] arrWord = getArrWord();
    String day = getDay(arrWord).trim().toLowerCase();
    switch (day) {
      case "monday":
        System.out.printf("Your tasks for %s: %s%n", arr[0][0], arr[0][1]);
        break;
      case "tuesday":
        System.out.printf("Your tasks for %s: %s%n", arr[1][0], arr[1][1]);
        break;
      case "wednesday":
        System.out.printf("Your tasks for %s: %s%n", arr[2][0], arr[2][1]);
        break;
      case "thursday":
        System.out.printf("Your tasks for %s: %s%n", arr[3][0], arr[3][1]);
        break;
      case "friday":
        System.out.printf("Your tasks for %s: %s%n", arr[4][0], arr[4][1]);
        break;
      case "saturday":
        System.out.printf("Your tasks for %s: %s%n", arr[5][0], arr[5][1]);
        break;
      case "sunday":
        System.out.printf("Your tasks for %s: %s%n", arr[6][0], arr[6][1]);
        break;
      case "change":
        changeTask(schedule, arrWord[1].trim().toLowerCase());
        break;
      case "exit":
        isContinue = false;
        break;
      default:
        System.out.println("Sorry, I don't understand you, please try again.\n");
    }
  }

  private static void changeTask(String[][] arr, String day) {
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < 1; j++) {
        if (day.equals(arr[i][j])) {
          System.out.printf("Please, input new tasks for %s.%n", arr[i]);
          Scanner scanner = new Scanner(System.in);
          arr[i][1] = scanner.nextLine();
          break;
        }
      }
    }
  }

  private static String getDay(String[]arr) {
    if (arr[0].equals(CHANGE) || arr[0].equals((RESCHEDULE))) {
      return "change";
    } else {
      return arr[0];
    }
  }
}
