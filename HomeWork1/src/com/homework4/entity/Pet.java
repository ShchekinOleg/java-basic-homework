package com.homework4.entity;

import com.homework4.enums.Species;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
  private Species species;
  private String nickname;
  private int age;
  private int trickLevel;
  private String[] habits;

  static {
    System.out.println("Static init block - Pet class");
  }

  {
    System.out.println("Init block - creating new Pet");
  }

  public Pet() {
  }

  public Pet(Species species, String nickname) {
    this.species = species;
    this.nickname = nickname;
  }

  public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
    this.species = species;
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  private void eat() {
    System.out.println("Я кушаю!");
  }

  private void respond() {
    System.out.printf("Привет, хозяин. Я - %s%n. Я соскучился!", this.nickname);
  }

  private void foul() {
    System.out.println("ужно хорошо замести следы...");
  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    if (trickLevel <= 0 || trickLevel > 100) {
      throw new RuntimeException("Invalid trickLevel");
    }
    this.trickLevel = trickLevel;
  }

  public String[] getHabits() {
    return habits;
  }

  public void setHabits(String[] habits) {
    this.habits = habits;
  }

  @Override
  public String toString() {
    return String.format("pet=dog{nickname='%s',age=%d,trickLevel=%d,habits=[%s]}",
      this.getNickname(), this.getAge(), this.getTrickLevel(), Arrays.toString(this.habits));
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Pet)) return false;
    Pet pet = (Pet) o;
    return getAge() == pet.getAge() &&
      getTrickLevel() == pet.getTrickLevel() &&
      Objects.equals(getSpecies(), pet.getSpecies()) &&
      Objects.equals(getNickname(), pet.getNickname()) &&
      Arrays.equals(getHabits(), pet.getHabits());
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(getSpecies(), getNickname(), getAge(), getTrickLevel());
    result = 31 * result + Arrays.hashCode(getHabits());
    return result;
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted", this.getClass());
  }
}