package com.homework4.entity;

import java.util.Arrays;
import java.util.Objects;

public class Human {
  private String name;
  private String surname;
  private int year;
  private int iq;
  private Pet pet;
  private Human mother;
  private Human father;
  private String[][] schedule;
  private Family family;

  static {
    System.out.println("Static init block - Human class");
  }

  {
    System.out.println("Init block - creating new Human");
  }

  public Human() {
  }

  public Human(String name, String surname, int year) {
    this.name = name;
    this.surname = surname;
    this.year = year;
  }

  public Human(String name, String surname, int year, Human mother, Human father) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.mother = mother;
    this.father = father;
  }

  public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule, Family family) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
    this.pet = pet;
    this.mother = mother;
    this.father = father;
    this.schedule = schedule;
    this.family = family;
  }

  private void greetPet() {
    System.out.printf("Привет, %s", this.pet.getNickname());
  }

  private void describePet() {
    System.out.printf("У меня есть %s, ему %d лет, он %s",
      this.pet.getSpecies(), this.pet.getAge(), this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    if (iq <= 0 || iq > 100) {
      throw new RuntimeException("Invalid iq");
    }
    this.iq = iq;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public String[][] getSchedule() {
    return schedule;
  }

  public void setSchedule(String[][] schedule) {
    this.schedule = schedule;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  @Override
  public String toString() {
    return String.format("Human{name=%s,surname=%s,year=%d", this.name, this.surname, this.year);
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Human)) return false;
    Human human = (Human) o;
    return year == human.year &&
      iq == human.iq &&
      Objects.equals(name, human.name) &&
      Objects.equals(surname, human.surname) &&
      Objects.equals(pet, human.pet) &&
      Objects.equals(mother, human.mother) &&
      Objects.equals(father, human.father) &&
      Arrays.equals(schedule, human.schedule) &&
      Objects.equals(family, human.family);
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(name, surname, year, iq, pet, mother, father, family);
    result = 31 * result + Arrays.hashCode(schedule);
    return result;
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted%n", this.getClass());
  }
}
