package com.homework4.enums;

public enum Species {
  Dog,
  Cat,
  Fish,
  Bird
}
