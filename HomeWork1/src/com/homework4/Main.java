package com.homework4;

import com.homework4.entity.Family;
import com.homework4.entity.Human;
import com.homework4.entity.Pet;
import com.homework4.enums.DayOfWeek;
import com.homework4.enums.Species;

import java.util.Arrays;

public class Main {
  public static void main(String[] args) {
    Human human1 = new Human("Sara", "Joy", 35);
    Human human2 = new Human("Tom", "Fort", 30);
    String[][] schedule = {
      {DayOfWeek.MONDAY.getName(), "do something"},
      {DayOfWeek.TUESDAY.getName(), "homework"},
      {DayOfWeek.WEDNESDAY.getName(), "study"},
      {DayOfWeek.THURSDAY.getName(), "one more time"},
      {DayOfWeek.FRIDAY.getName(), "party time"},
      {DayOfWeek.SATURDAY.getName(), "have a fun"},
      {DayOfWeek.SUNDAY.getName(), "take a break"}
    };
    human1.setSchedule(schedule);

    Family family = new Family(human1, human2);

    System.out.println(human1.toString());

    Human child1 = new Human("John", "Fort", 25);
    Human child2 = new Human("Steve", "Fort", 15);
    Human child3 = new Human("Ann", "Fort", 14);

    Human[] children = {child1, child2, child3};

    family.setChildren(children);

    Pet pet = new Pet(Species.Dog, "Drl");
    family.setPet(pet);
    System.out.println(family.toString());
    System.out.printf("The family consists from %d humans%n", family.countFamilyMember(family));
    family.deleteChild(children);
    System.out.println(family.toString());
    System.out.printf("The family consists from %d humans%n", family.countFamilyMember(family));
    Human child4 = new Human("Liza", "Fort", 1);
    family.addChild(children, child4);
    System.out.println(Arrays.toString(family.getChildren()));
    System.out.println(family.toString());
    System.out.printf("The family consists from %d humans%n", family.countFamilyMember(family));
//    for (int i = 0; i < 1000000; i++) {
//      Human human = new  Human();
//    }
  }


}
