package com.homework8.entity.family;

import com.homework8.entity.pet.Pet;

import java.util.Map;
import java.util.Objects;

public class Human {
  private String name;
  private String surname;
  private int year;
  private int iq;
  private Pet pet;
  private Human mother;
  private Human father;
  private Map<String, String> schedule;
  private Family family;

  static {
    System.out.println("Static init block - Human class");
  }

  {
    System.out.println("Init block - creating new Human");
  }

  public Human() {
  }

  public Human(String name, String surname, int year) {
    this.name = name;
    this.surname = surname;
    this.year = year;
  }

  public Human(String name, String surname, int year, Human mother, Human father) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.mother = mother;
    this.father = father;
  }

  public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule, Family family) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
    this.pet = pet;
    this.mother = mother;
    this.father = father;
    this.schedule = schedule;
    this.family = family;
  }

  private void greetPet() {
    System.out.printf("Привет, %s", this.pet.getNickname());
  }

  private void describePet() {
    System.out.printf("У меня есть %s, ему %d лет, он %s",
      this.pet.getSpecies(), this.pet.getAge(), this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    if (iq <= 0 || iq > 100) {
      throw new RuntimeException("Invalid iq");
    }
    this.iq = iq;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Map<String, String> getSchedule() {
    return schedule;
  }

  public void setSchedule(Map<String, String> schedule) {
    this.schedule = schedule;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  @Override
  public String toString() {
    return String.format("Human{name=%s,surname=%s,year=%d", this.name, this.surname, this.year);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Human)) return false;
    Human human = (Human) o;
    return getYear() == human.getYear() &&
      getIq() == human.getIq() &&
      Objects.equals(getName(), human.getName()) &&
      Objects.equals(getSurname(), human.getSurname()) &&
      Objects.equals(getPet(), human.getPet()) &&
      Objects.equals(getMother(), human.getMother()) &&
      Objects.equals(getFather(), human.getFather()) &&
      Objects.equals(getSchedule(), human.getSchedule()) &&
      Objects.equals(getFamily(), human.getFamily());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getName(), getSurname(), getYear(), getIq(), getPet(), getMother(), getFather(), getSchedule(), getFamily());
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted%n", this.getClass());
  }
}
