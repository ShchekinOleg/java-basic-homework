package com.homework8.entity.family;

import com.homework8.entity.pet.Pet;

import java.util.*;
import java.util.stream.Collectors;

public class Family {
  private Human human1;
  private Human human2;
  private List<Human> children;
  private Set<Pet> pet;

  static {
    System.out.println("Static init block - Family class");
  }

  {
    System.out.println("Init block - creating new Family");
  }

  public Family(Human human1, Human human2) {
    this.human1 = human1;
    this.human2 = human2;
  }

  public Family(Human human1, Human human2, List<Human> children) {
    this.human1 = human1;
    this.human2 = human2;
    this.children = children;
  }

  public Family() {
  }

  public Family(Human human1, Human human2, List<Human> children, Set<Pet> pet) {
    this.human1 = human1;
    this.human2 = human2;
    this.children = children;
    this.pet = pet;
  }


  public boolean isChildOlder18(List<Human> children) {
    long count = children.stream().filter(c -> c.getYear() > 18).count();
    boolean isOlder;
    if (count != 0 ? true : false) isOlder = true;
    else isOlder = false;
    return isOlder;
  }

  public void deleteChild(List<Human> children, int age) {
    children = children.stream().filter(c -> c.getYear() < age).collect(Collectors.toList());
    setChildren(children);
  }

  public int countFamilyMember(Family family) {
    int counter = 2;
    return counter + family.getChildren().size();
  }

  public void addChild(Family family, Human child) {
    if (children == null){
      List<Human>childrenList = new ArrayList<>();
      childrenList.add(child);
      family.setChildren(childrenList);
    }else {
      children.add(child);
      family.setChildren(children);
    }

  }

  public void bornChild(Family family, String girlName, String boyName) {
    Random random = new Random();
    Human human;
    if (random.nextBoolean()) {
      human = new Woman(girlName, family.getHuman1().getSurname(), 0);
    } else {
      human = new Man(boyName, family.getHuman1().getSurname(), 0);
    }
    family.addChild(family, human);
  }

  public Human getHuman1() {
    return human1;
  }

  public void setHuman1(Human human1) {
    this.human1 = human1;
  }

  public Human getHuman2() {
    return human2;
  }

  public void setHuman2(Human human2) {
    this.human2 = human2;
  }

  public List<Human> getChildren() {
    return children;
  }

  private void setChildren(List<Human> children) {
    this.children = children;
  }

  public Set<Pet> getPet() {
    return pet;
  }

  public void setPet(Set<Pet> pet) {
    this.pet = pet;
  }

  public void addPet(Set<Pet> pets, Pet pet) {
    pets.add(pet);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Family)) return false;
    Family family = (Family) o;
    return Objects.equals(getHuman1(), family.getHuman1()) &&
      Objects.equals(getHuman2(), family.getHuman2()) &&
      Objects.equals(getChildren(), family.getChildren()) &&
      Objects.equals(getPet(), family.getPet());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getHuman1(), getHuman2(), getChildren(), getPet());
  }

  @Override
  public String toString() {
    return "Family{" +
      "human1=" + human1 +
      ", human2=" + human2 +
      ", children=" + children +
      ", pet=" + pet +
      '}';
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted", this.getClass());
  }


}
