package com.homework8;


import com.homework8.controler.FamilyController;
import com.homework8.dao.FamilyDao;
import com.homework8.dao.daoImpl.DaoFactoryImpl;
import com.homework8.entity.family.Human;
import com.homework8.service.FamilyService;

public class Main {
  public static void main(String[] args) {
    FamilyDao familyDAO = DaoFactoryImpl.createFamilyDao();
    FamilyService familyService = new FamilyService(familyDAO);
    FamilyController familyController = new FamilyController(familyService);

    Human human1 = new Human("Brad", "Pitt", 30);
    Human human2 = new Human("Samanta", "Pitt", 30);
    familyController.createNewFamily(human1, human2);

    Human human3 = new Human("Duck", "Smit", 35);
    Human human4 = new Human("Li", "Smit", 30);
    familyController.createNewFamily(human3, human4);

    System.out.println(familyController.getAllFamilies());

    familyController.bornChild(familyController.getFamilyByIndex(0), "Roc", "Rosa");
    System.out.println(familyController.getFamilyByIndex(0));

    familyController.adoptChild(familyController.getFamilyByIndex(1), new Human("Olga", "Swip", 5));
    familyController.adoptChild(familyController.getFamilyByIndex(1), new Human("Rita", "Swip", 10));
    System.out.println(familyController.getFamilyByIndex(1));

    System.out.println(familyController.count());
    System.out.println(familyController.countFamiliesWithMemberNumber(4));
    System.out.println(familyController.countFamiliesWithMemberNumber(3));

    familyController.getFamiliesBiggerThan(2);
    familyController.getFamiliesBiggerThan(3);
    familyController.getFamiliesBiggerThan(4);
    familyController.getFamiliesBiggerThan(5);

    familyController.getFamiliesLessThan(2);
    familyController.getFamiliesLessThan(3);
    familyController.getFamiliesLessThan(4);
    familyController.getFamiliesLessThan(5);


    familyController.displayAllFamilies();
    familyController.deleteAllChildrenOlderThen(7);
    familyController.displayAllFamilies();
    familyController.deleteFamilyByIndex(0);
    familyController.displayAllFamilies();
    System.out.println(familyController.getFamilyByIndex(0));

  }
}
