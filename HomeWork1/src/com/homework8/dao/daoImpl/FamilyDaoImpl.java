package com.homework8.dao.daoImpl;

import com.homework8.dao.FamilyDao;
import com.homework8.entity.family.Family;

import java.util.ArrayList;
import java.util.List;

public class FamilyDaoImpl implements FamilyDao {
  private  List<Family> families = new ArrayList<>();

  public FamilyDaoImpl() {
  }

  @Override
  public List<Family> getAllFamilies() {
    return families;
  }

  @Override
  public Family getFamilyByIndex(int i) {
    return families.get(i);
  }

  @Override
  public void deleteFamily(Family family) {
    families.remove(family);
  }

  @Override
  public void deleteFamily(int i) {
    families.remove(i);
  }

  @Override
  public void saveFamily(Family family) {
    families.add(family);
  }

  @Override
  public void create(Family object) {

  }

  @Override
  public Family getById(int id) {
    return null;
  }

  @Override
  public List<Family> getAll() {
    return null;
  }

  @Override
  public void update(Family object) {

  }

  @Override
  public void delete(Family object) {

  }

  @Override
  public void close() {

  }
}
