package com.homework8.dao.daoImpl;

import com.homework8.dao.DaoFactory;
import com.homework8.dao.FamilyDao;

public final class DaoFactoryImpl extends DaoFactory {
  public static FamilyDao createFamilyDao() {
    return new FamilyDaoImpl() {
    };
  }
}
