package com.homework8.dao;

import com.homework8.entity.family.Family;

import java.util.List;

public interface FamilyDao extends DAO<Family> {
  List<Family> getAllFamilies();

  Family getFamilyByIndex(int i);

  void deleteFamily(Family family);

  void deleteFamily(int i);

  void saveFamily(Family family);
}
