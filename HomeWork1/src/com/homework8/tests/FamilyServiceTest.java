package com.homework8.tests;

import com.homework8.entity.family.Human;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FamilyServiceTest {
  Human human1;
  Human human2;
  Human human3;
  Human human4;

  @BeforeEach
  void setUp() {
    human1 = new Human("Brad", "Pitt", 30);
    human2 = new Human("Samanta", "Pitt", 30);
    human3 = new Human("Duck", "Smit", 35);
    human4 = new Human("Li", "Smit", 30);
  }

  @AfterEach
  void tearDown() {
    human1 = null;
    human2 = null;
    human3 = null;
    human4 = null;
  }

  @Test
  void getAllFamilies() {
  }

  @Test
  void displayAllFamilies() {
  }

  @Test
  void getFamiliesBiggerThan() {
  }

  @Test
  void getFamiliesLessThan() {
  }

  @Test
  void countFamiliesWithMemberNumber() {
  }

  @Test
  void createNewFamily() {
  }

  @Test
  void deleteFamilyByIndex() {
  }

  @Test
  void bornChild() {
  }

  @Test
  void adoptChild() {
  }

  @Test
  void deleteAllChildrenOlderThen() {
  }

  @Test
  void count() {
  }

  @Test
  void getFamilyByIndex() {
  }

  @Test
  void addPet() {
  }

  @Test
  void getPets() {
  }
}