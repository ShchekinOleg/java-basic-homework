package com.homework8.enums;

public enum Species {
  DOG,
  FISH,
  DOMESTICCAT,
  ROBOCAT,
  UNKNOWN
}
