package com.homework8.controler;

import com.homework8.entity.family.Family;
import com.homework8.entity.family.Human;
import com.homework8.entity.pet.Pet;
import com.homework8.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
  private FamilyService familyService;

  public FamilyController(FamilyService familyService) {
    this.familyService = familyService;
  }

  public List<Family> getAllFamilies() {
    return familyService.getAllFamilies();
  }

  public void displayAllFamilies() {
    familyService.displayAllFamilies();
  }

  public List<Family> getFamiliesBiggerThan(int number) {
    return familyService.getFamiliesBiggerThan(number);
  }

  public List<Family> getFamiliesLessThan(int number) {
    return familyService.getFamiliesLessThan(number);
  }

  public int countFamiliesWithMemberNumber(int number) {
    return familyService.countFamiliesWithMemberNumber(number);
  }

  public void createNewFamily(Human human1, Human human2) {
    familyService.createNewFamily(human1, human2);
  }

  public void deleteFamilyByIndex(int index) {
    familyService.deleteFamilyByIndex(index);
  }

  public void bornChild(Family family, String female, String male) {
    familyService.bornChild(family, male, female);
  }

  public Family adoptChild(Family family, Human human) {
    return familyService.adoptChild(family, human);
  }

  public void deleteAllChildrenOlderThen(int age) {
    familyService.deleteAllChildrenOlderThen(age);
  }

  public int count() {
    return familyService.count();
  }

  public Family getFamilyByIndex(int index) {
    return familyService.getFamilyByIndex(index);
  }

  public Set<Pet> getPets(int index) {
    return familyService.getPets(index);
  }

  public void addPet(int index, Set<Pet> pets, Pet pet) {
    familyService.addPet(index, pets, pet);
  }
}
