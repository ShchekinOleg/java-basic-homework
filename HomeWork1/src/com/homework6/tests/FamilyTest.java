package com.homework4.tests;

import com.homework4.entity.Family;
import com.homework4.entity.Human;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
  Human human1;
  Human human2;
  Human child1;
  Human child2;
  Human child3;
  Human[] children;
  Family family;

  @BeforeEach
  void setUp() {
    human1 = new Human("Sara", "Joy", 35);
    human2 = new Human("Tom", "Fort", 30);

    child1 = new Human("John", "Fort", 25);
    child2 = new Human("Steve", "Fort", 15);
    child3 = new Human("Ann", "Fort", 14);

    children = new Human[]{child1, child2, child3};
    family = new Family(human1, human2, children);
  }

  @AfterEach
  void tearDown() {
    human1 = null;
    human2 = null;
    child1 = null;
    child2 = null;
    child3 = null;

    children = null;
    family = null;
  }

  @Test
  void isChildOlder18() {
    assertTrue(child1.getYear() > 18);
  }

  @Test
  void deleteChild() {
    family.deleteChild(children);
    assertNotEquals(5, family.countFamilyMember(family));
    assertEquals(4, family.countFamilyMember(family));
    for (int i = 0; i < children.length; i++) {
      assertTrue(children[i].getYear() < 18);
      assertFalse(children[i].getYear() >= 18);
    }
  }

  @Test
  void countFamilyMember() {
    assertEquals(5, 2 + children.length);
  }

  @Test
  void addChild() {
    Human child5 = new Human("Greta", "Fort", 1);
    family.addChild(children, child5);
    assertEquals(5, family.countFamilyMember(family));
  }
}