package com.homework6.enums;

public enum Species {
  DOG,
  FISH,
  DOMESTICCAT,
  ROBOCAT,
  UNKNOWN
}
