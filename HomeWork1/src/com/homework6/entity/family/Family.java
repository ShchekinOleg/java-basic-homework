package com.homework6.entity.family;

import com.homework6.entity.pet.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Family {
  private Human human1;
  private Human human2;
  private Human[] children;
  private Pet pet;

  static {
    System.out.println("Static init block - Family class");
  }

  {
    System.out.println("Init block - creating new Family");
  }

  public Family(Human human1, Human human2) {
    this.human1 = human1;
    this.human2 = human2;
  }

  public Family(Human human1, Human human2, Human[] children) {
    this.human1 = human1;
    this.human2 = human2;
    this.children = children;
  }

  public Family() {
  }

  public Family(Human human1, Human human2, Human[] children, Pet pet) {
    this.human1 = human1;
    this.human2 = human2;
    this.children = children;
    this.pet = pet;
  }


  public boolean isChildOlder18(Human[] children) {
    if (children.length > 0) {
      for (Human child : children) {
        if (child.getYear() > 18) return true;
      }
    }
    return true;
  }

  private Human[] newChildrenSet;

  public Human[] createArr(Human[] children) {
    newChildrenSet = new Human[children.length - 1];
    int i;
    for (i = 0; i < children.length; i++) {
      if (children[i].getYear() > 18) {
        break;
      }
    }
    for (int j = i; j < children.length - 1; j++) {
      children[j] = children[j + 1];
    }
    System.arraycopy(children, 0, newChildrenSet, 0, newChildrenSet.length);
    return newChildrenSet;
  }

  public void deleteChild(Human[] children) {
    if (isChildOlder18(children)) {
      createArr(children);
      children = null;
      setChildren(newChildrenSet);
    }
  }

  public int countFamilyMember(Family family) {
    int counter = 2;
    return counter + family.getChildren().length;
  }

  public void addChild(Human[] children, Human child) {
    Human[] addOneChild;
    addOneChild = Arrays.copyOf(children, children.length);
    addOneChild[addOneChild.length - 1] = child;
    setChildren(addOneChild);
  }

  public Human getHuman1() {
    return human1;
  }

  public void setHuman1(Human human1) {
    this.human1 = human1;
  }

  public Human getHuman2() {
    return human2;
  }

  public void setHuman2(Human human2) {
    this.human2 = human2;
  }

  public Human[] getChildren() {
    return children;
  }

  public void setChildren(Human[] children) {
    this.children = children;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public String toString() {
    return String.format("Family{mother %s, father %s, children= %s, pet=%s",
      this.human1.toString(), this.human2.toString(), Arrays.toString(this.getChildren()), this.pet.toString());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Family)) return false;
    Family family = (Family) o;
    return Objects.equals(getHuman1(), family.getHuman1()) &&
      Objects.equals(getHuman2(), family.getHuman2()) &&
      Arrays.equals(getChildren(), family.getChildren()) &&
      Objects.equals(getPet(), family.getPet());
  }

  @Override
  public int hashCode() {

    int result = Objects.hash(getHuman1(), getHuman2(), getPet());
    result = 31 * result + Arrays.hashCode(getChildren());
    return result;
  }

  public Human[] getChildren(Human[] children) {
    return children;
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted", this.getClass());
  }
}
