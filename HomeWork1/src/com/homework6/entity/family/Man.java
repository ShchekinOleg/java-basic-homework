package com.homework6.entity.family;

import com.homework6.entity.pet.Pet;

public class Man extends Human {
  public Man() {
  }

  public Man(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Man(String name, String surname, int year, Human mother, Human father) {
    super(name, surname, year, mother, father);
  }

  public Man(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule, Family family) {
    super(name, surname, year, iq, pet, mother, father, schedule, family);
  }

}
