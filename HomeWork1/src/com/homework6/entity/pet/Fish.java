package com.homework6.entity.pet;


public class Fish extends Pet {
  public Fish(String nickname) {
    super(nickname);
  }

  public Fish(String nickname, int age, int trickLevel, String[] habits) {
    super(nickname, age, trickLevel, habits);
  }

  @Override
  public void respond() {
    System.out.printf("Привет, хозяин. Я - %s%n. Я соскучился!", getNickname());
  }
}
