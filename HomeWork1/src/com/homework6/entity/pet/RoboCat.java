package com.homework6.entity.pet;

public class RoboCat extends Pet {
  @Override
  public void respond() {
    System.out.printf("Привет, хозяин. Я - %s%n. Я соскучился!", getNickname());
  }
}
