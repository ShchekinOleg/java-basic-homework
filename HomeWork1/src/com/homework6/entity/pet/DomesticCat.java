package com.homework6.entity.pet;

public class DomesticCat extends Pet implements Foul{
  @Override
  public void respond() {
    System.out.printf("Привет, хозяин. Я - %s%n. Я соскучился!", getNickname());
  }

  @Override
  public void foul() {
    System.out.println("Нужно хорошо замести следы...");
  }
}
