package com.homework1;

import java.util.Random;
import java.util.Scanner;

public class HomeWork1_1 {
  public static void main(String[] args) {
    String arr[][] = {
      {"1941", "World War 1"},
      {"1945", "World War 2"},
      {"1952", "Invention of the internet"},
      {"1969", "First flight to the moon "},
      {"1986", "Chernobyl disaster"},
      {"1995", "Java invention"}
    };
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        System.out.print(arr[i][j] + "\t");
      }
      System.out.println();
    }
    Random random = new Random();
    int number = random.nextInt(arr.length);
    System.out.println(arr[number][1]);

    Scanner scanner = new Scanner(System.in);
    String answer = scanner.next();
    if (answer.equals(arr[number][0])) {
      System.out.println("You are right.\nCorrect answer is:");
      for (int i = number; i == number; i++) {
        for (int j = 0; j < arr[i].length; j++) {
          System.out.print(arr[i][j] + "\t");
        }
      }

    }else {
      System.out.println("You are wrong");
    }
  }
}
