package com.homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HomeWork1 {
  public static void main(String[] args) {
    Random random = new Random();
    int randomNumber = random.nextInt(101);
    System.out.printf("(Random number is: %d\n", randomNumber);
    Scanner scanner = new Scanner(System.in);
    System.out.println("What is your name?\n");
    String name = scanner.nextLine();

    System.out.printf("Let the game begin, %str!\n", name);
    String str;
    int number;
    int counter = 0;
    int arr[] = new int[1000];
    do {
      do {
        str = scanner.next();
        if (!str.matches("\\d+")) {
          System.out.println("Not a number. Try again");
        }
      } while (!str.matches("\\d+"));
      number = Integer.valueOf(str);
      arr[counter] = number;
      counter++;
      if (number < randomNumber) {
        System.out.println("Your number is too small. Please, try again.");
      } else if (number > randomNumber) {
        System.out.println("Your number is too big. Please, try again.");
      } else {
        System.out.printf("Congratulations, %s! The correct number is %d. You guessed it in %d tries. \n", name, randomNumber, counter);
      }
    } while (number != randomNumber);
    int[] numbers = Arrays.copyOf(arr, counter);
    Arrays.sort(numbers);
    System.out.println("Your entered number is:");
    for (int num : numbers) {
      System.out.print(num + " ");
    }
  }
}
