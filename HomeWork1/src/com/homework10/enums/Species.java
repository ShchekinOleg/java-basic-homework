package com.homework10.enums;

public enum Species {
  DOG,
  FISH,
  DOMESTICCAT,
  ROBOCAT,
  UNKNOWN
}
