package com.homework10.entity.pet;


import com.homework10.enums.Species;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {
  private Species species;
  private String nickname;
  private int age;
  private int trickLevel;
  private Set<String> habits;

  static {
    System.out.println("Static init block - Pet class");
  }

  {
    System.out.println("Init block - creating new Pet");
  }

  {
    species = Species.valueOf(this.getClass().getSimpleName().toUpperCase());
  }

  public Pet() {
  }

  public Pet(String nickname) {
    this.nickname = nickname;
  }

  public Pet(Species species, String nickname) {
    this.species = species;
    this.nickname = nickname;
  }

  public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  private void eat() {
    System.out.println("Я кушаю!");
  }

  public abstract void respond();

  private void foul() {
    System.out.println("ужно хорошо замести следы...");
  }

  public Species getSpecies() {
    return species;
  }

  public void setSpecies(Species species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    if (trickLevel <= 0 || trickLevel > 100) {
      throw new RuntimeException("Invalid trickLevel");
    }
    this.trickLevel = trickLevel;
  }

  public void setHabits(Set<String> habits) {
    this.habits = habits;
  }

  public Set<String> getHabits() {
    return habits;
  }

  @Override
  public String toString() {
    return "Pet{" +
      "species=" + species +
      ", nickname='" + nickname + '\'' +
      ", age=" + age +
      ", trickLevel=" + trickLevel +
      ", habits=" + habits +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Pet)) return false;
    Pet pet = (Pet) o;
    return getAge() == pet.getAge() &&
      getTrickLevel() == pet.getTrickLevel() &&
      getSpecies() == pet.getSpecies() &&
      Objects.equals(getNickname(), pet.getNickname()) &&
      Objects.equals(getHabits(), pet.getHabits());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getSpecies(), getNickname(), getAge(), getTrickLevel(), getHabits());
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted", this.getClass());
  }
}