package com.homework10.entity.family;

import com.homework10.entity.pet.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {
  private String name;
  private String surname;
  private long birthDate;
  private int iq;
  private Pet pet;
  private Human mother;
  private Human father;
  private Map<String, String> schedule;
  private Family family;

  static {
    System.out.println("Static init block - Human class");
  }

  {
    System.out.println("Init block - creating new Human");
  }

  public Human() {
  }

  public Human(String name, String surname, String birthDate) {
    this.name = name;
    this.surname = surname;
    this.birthDate = convertDateToLong(birthDate);
  }

  public Human(String name, String surname, String birthDate, Human mother, Human father) {
    this.name = name;
    this.surname = surname;
    this.birthDate = convertDateToLong(birthDate);
    this.mother = mother;
    this.father = father;
  }

  public Human(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule, Family family) {
    this.name = name;
    this.surname = surname;
    this.birthDate = convertDateToLong(birthDate);
    this.iq = iq;
    this.pet = pet;
    this.mother = mother;
    this.father = father;
    this.schedule = schedule;
    this.family = family;
  }

  public long convertDateToLong(String dateString) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    try {
      return simpleDateFormat.parse(dateString).getTime();
    } catch (ParseException e) {
      return System.currentTimeMillis();
    }
  }

  private void greetPet() {
    System.out.printf("Привет, %s", this.pet.getNickname());
  }

  private void describePet() {
    System.out.printf("У меня есть %s, ему %d лет, он %s",
      this.pet.getSpecies(), this.pet.getAge(), this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public long getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(long birthDate) {
    this.birthDate = birthDate;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    if (iq <= 0 || iq > 100) {
      throw new RuntimeException("Invalid iq");
    }
    this.iq = iq;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Map<String, String> getSchedule() {
    return schedule;
  }

  public void setSchedule(Map<String, String> schedule) {
    this.schedule = schedule;
  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  @Override
  public String toString() {
    Date localDateTime = new Date(birthDate);
    return "Human{" +
      "name='" + name + '\'' +
      ", surname='" + surname + '\'' +
      ", birthDate=" + localDateTime +
      ", iq=" + iq +
      ", pet=" + pet +
      ", mother=" + mother +
      ", father=" + father +
      ", schedule=" + schedule +
      ", family=" + family +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Human)) return false;
    Human human = (Human) o;
    return getBirthDate() == human.getBirthDate() &&
      getIq() == human.getIq() &&
      Objects.equals(getName(), human.getName()) &&
      Objects.equals(getSurname(), human.getSurname()) &&
      Objects.equals(getPet(), human.getPet()) &&
      Objects.equals(getMother(), human.getMother()) &&
      Objects.equals(getFather(), human.getFather()) &&
      Objects.equals(getSchedule(), human.getSchedule()) &&
      Objects.equals(getFamily(), human.getFamily());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getName(), getSurname(), getBirthDate(), getIq(), getPet(), getMother(), getFather(), getSchedule(), getFamily());
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    System.out.printf("The %s object will be deleted%n", this.getClass());
  }
}
