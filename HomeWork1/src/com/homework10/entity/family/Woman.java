package com.homework10.entity.family;

import com.homework10.entity.pet.Pet;

import java.util.Map;

public class Woman extends Human {
  public Woman() {
  }

  public Woman(String name, String surname, String birthDate) {
    super(name, surname, birthDate);
  }

  public Woman(String name, String surname, String birthDate, Human mother, Human father) {
    super(name, surname, birthDate, mother, father);
  }

  public Woman(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule, Family family) {
    super(name, surname, birthDate, iq, pet, mother, father, schedule, family);
  }
}
