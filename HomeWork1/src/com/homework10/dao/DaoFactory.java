package com.homework10.dao;


import com.homework10.dao.daoImpl.DaoFactoryImpl;

public abstract class DaoFactory {

  private static DaoFactory daoFactory;

  public static FamilyDao createFamilyDao() {
    return null;
  }

  public static DaoFactory getInstance(){
    if(daoFactory == null){
      synchronized (DaoFactory.class){
        if(daoFactory == null){
          daoFactory = new DaoFactoryImpl();
        }
      }
    }
    return daoFactory;
  }
}
