package com.homework10.dao.daoImpl;

import com.homework10.dao.DaoFactory;
import com.homework10.dao.FamilyDao;

public final class DaoFactoryImpl extends DaoFactory {
  public static FamilyDao createFamilyDao() {
    return new FamilyDaoImpl() {
    };
  }
}
