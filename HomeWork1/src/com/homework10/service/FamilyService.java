package com.homework10.service;

import com.homework10.dao.FamilyDao;
import com.homework10.entity.family.Family;
import com.homework10.entity.family.Human;
import com.homework10.entity.pet.Pet;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
  private FamilyDao familyDao;

  public FamilyService(FamilyDao familyDao) {
    this.familyDao = familyDao;
  }

  public List<Family> getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  public void displayAllFamilies() {
    familyDao.getAllFamilies().forEach(System.out::println);
  }

  public List<Family> getFamiliesBiggerThan(int quantity) {
    return familyDao.getAllFamilies()
      .stream()
      .filter(f -> f.getChildren().size() + 2 > quantity)
      .collect(Collectors.toList());
  }

  public List<Family> getFamiliesLessThan(int quantity) {
    return familyDao.getAllFamilies()
      .stream()
      .filter(f -> f.getChildren().size() + 2 < quantity)
      .collect(Collectors.toList());
  }

  public long countFamiliesWithMemberNumber(int quantity) {
    return familyDao.getAllFamilies()
      .stream()
      .filter(f -> f.getChildren().size() + 2 == quantity)
      .count();
  }

  public void createNewFamily(Human human1, Human human2) {
    familyDao.saveFamily(new Family(human1, human2));
  }

  public void deleteFamilyByIndex(int index) {
    if (index < familyDao.getAllFamilies().size()) {
      familyDao.deleteFamily(index);
    }
  }

  public void bornChild(Family family, String human1, String human2) {
    family.bornChild(family, human1, human2);
  }

  public Family adoptChild(Family family, Human human) {
    family.addChild(family, human);
    return family;
  }

  public void deleteAllChildrenOlderThen(int age) {
    familyDao.getAllFamilies()
      .stream()
      .peek(e -> e.getChildren().removeIf(c -> c.getBirthDate() > age))
      .collect(Collectors.toList());

  }

  public int count() {
    return familyDao.getAllFamilies().size();
  }

  public Family getFamilyByIndex(int index) {
    return familyDao.getFamilyByIndex(index);
  }

  public void addPet(int index, Set<Pet> pets, Pet pet) {
    familyDao.getFamilyByIndex(index).addPet(pets, pet);
  }

  public Set<Pet> getPets(int index) {
    return familyDao.getFamilyByIndex(index).getPet();
  }
}
