package com.homework7.enums;

public enum Species {
  DOG,
  FISH,
  DOMESTICCAT,
  ROBOCAT,
  UNKNOWN
}
