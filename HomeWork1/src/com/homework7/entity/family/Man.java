package com.homework7.entity.family;

import com.homework7.entity.pet.Pet;

import java.util.Map;

public class Man extends Human {
  public Man() {
  }

  public Man(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Man(String name, String surname, int year, Human mother, Human father) {
    super(name, surname, year, mother, father);
  }

  public Man(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule, Family family) {
    super(name, surname, year, iq, pet, mother, father, schedule, family);
  }

}
