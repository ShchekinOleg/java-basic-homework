package com.homework9.service;

import com.homework9.dao.FamilyDao;
import com.homework9.entity.family.Family;
import com.homework9.entity.family.Human;
import com.homework9.entity.pet.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
  private FamilyDao familyDao;

  public FamilyService(FamilyDao familyDao) {
    this.familyDao = familyDao;
  }

  public List<Family> getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  public void displayAllFamilies() {
    System.out.println(familyDao.getAllFamilies().toString());
  }

  public List<Family> getFamiliesBiggerThan(int quantity) {
    List<Family> searchedFamily = new ArrayList<>();
    for (Family family : familyDao.getAllFamilies()) {
      if (family.countFamilyMember(family) > quantity) {
        System.out.println(family);
        searchedFamily.add(family);
      }
    }
    return searchedFamily;
  }

  public List<Family> getFamiliesLessThan(int quantity) {
    List<Family> searchedFamily = new ArrayList<>();
    for (Family family : familyDao.getAllFamilies()) {
      if (family.countFamilyMember(family) < quantity) {
        System.out.println(family);
        searchedFamily.add(family);
      }
    }
    return searchedFamily;
  }

  public int countFamiliesWithMemberNumber(int quantity) {
    int count = 0;
    for (Family family : familyDao.getAllFamilies()) {
      if (family.countFamilyMember(family) == quantity) {
        count++;
      }
    }
    return count;
  }

  public void createNewFamily(Human human1, Human human2) {
    familyDao.saveFamily(new Family(human1, human2));
  }

  public void deleteFamilyByIndex(int index) {
    if (index < familyDao.getAllFamilies().size()) {
      familyDao.deleteFamily(index);
    }
  }

  public void bornChild(Family family, String human1, String human2) {
    family.bornChild(family, human1, human2);
  }

  public Family adoptChild(Family family, Human human) {
    family.addChild(family, human);
    return family;
  }

  public void deleteAllChildrenOlderThen(int age) {
    for (Family family : familyDao.getAllFamilies()) {
      family.deleteChild(family.getChildren(), age);
    }
  }

  public int count() {
    return familyDao.getAllFamilies().size();
  }

  public Family getFamilyByIndex(int index) {
    return familyDao.getFamilyByIndex(index);
  }

  public void addPet(int index, Set<Pet> pets, Pet pet) {
    familyDao.getFamilyByIndex(index).addPet(pets, pet);
  }

  public Set<Pet> getPets(int index) {
    return familyDao.getFamilyByIndex(index).getPet();
  }
}
