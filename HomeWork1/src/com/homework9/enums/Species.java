package com.homework9.enums;

public enum Species {
  DOG,
  FISH,
  DOMESTICCAT,
  ROBOCAT,
  UNKNOWN
}
