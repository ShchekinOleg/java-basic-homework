package com.homework9.dao;


import com.homework9.dao.daoImpl.DaoFactoryImpl;

public abstract class DaoFactory {

  private static DaoFactory daoFactory;

  public static FamilyDao createFamilyDao() {
    return null;
  }

  public static DaoFactory getInstance(){
    if(daoFactory == null){
      synchronized (DaoFactory.class){
        if(daoFactory == null){
          daoFactory = new DaoFactoryImpl();
        }
      }
    }
    return daoFactory;
  }
}
