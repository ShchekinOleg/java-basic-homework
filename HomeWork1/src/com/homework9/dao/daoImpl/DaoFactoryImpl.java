package com.homework9.dao.daoImpl;

import com.homework9.dao.DaoFactory;
import com.homework9.dao.FamilyDao;

public final class DaoFactoryImpl extends DaoFactory {
  public static FamilyDao createFamilyDao() {
    return new FamilyDaoImpl() {
    };
  }
}
