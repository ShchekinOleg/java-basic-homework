package com.homework9.entity.family;

import com.homework9.entity.pet.Pet;

import java.util.Map;

public class Man extends Human {
  public Man() {
  }

  public Man(String name, String surname, String birthDate) {
    super(name, surname, birthDate);
  }

  public Man(String name, String surname, String birthDate, Human mother, Human father) {
    super(name, surname, birthDate, mother, father);
  }

  public Man(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule, Family family) {
    super(name, surname, birthDate, iq, pet, mother, father, schedule, family);
  }

}
