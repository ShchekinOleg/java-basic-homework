package com.homework9;


import com.homework9.controler.FamilyController;
import com.homework9.dao.FamilyDao;
import com.homework9.dao.daoImpl.DaoFactoryImpl;
import com.homework9.entity.family.Human;
import com.homework9.service.FamilyService;

public class Main {
  public static void main(String[] args) {
    FamilyDao familyDAO = DaoFactoryImpl.createFamilyDao();
    FamilyService familyService = new FamilyService(familyDAO);
    FamilyController familyController = new FamilyController(familyService);

    Human human1 = new Human("Brad", "Pitt", "11/11/1980");
    Human human2 = new Human("Samanta", "Pitt", "04/02/1990");
    familyController.createNewFamily(human1, human2);

    Human human3 = new Human("Duck", "Smit", "21/12/1992");
    Human human4 = new Human("Li", "Smit", "05/12/2000");
    familyController.createNewFamily(human3, human4);

    System.out.println(familyController.getAllFamilies());
    System.out.println(familyController.getFamilyByIndex(0));

    familyController.bornChild(familyController.getFamilyByIndex(0), "Steve", "Liza");
    System.out.println(familyController.getFamilyByIndex(0));
  }
}
