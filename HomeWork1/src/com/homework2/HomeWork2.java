package com.homework2;

import java.util.Random;
import java.util.Scanner;

public class HomeWork2 {
  static int size = getMatrixSize();
  static String[][] matrix = createMatrix(size, size, "-");
  static int[] target = randomTarget();

  public static void run() {

    int[] shot = takeShot();
    int shot0 = shot[0] + 1;
    int shot1 = shot[1] + 1;
    int target0 = target[0];
    int target1 = target[1];
    if (target0 == shot0 && target1 == shot1) {
      matrix[shot[0]][shot[1]] = "x";
      System.out.println("You have won!");
      printMatrix(matrix);
    } else {
      matrix[shot[0]][shot[1]] = "*";
      printMatrix(matrix);
      run();
    }
  }

  public static void main(String[] args) {
    printMatrix(matrix);
    run();
  }

  public static String[][] createMatrix(int i, int j, String s) {
    String[][] arr = new String[i][j];
    for (int k = 0; k < arr.length; k++) {
      for (int l = 0; l < arr[k].length; l++) {
        arr[k][l] = s;
      }
    }
    return arr;
  }

  public static void printMatrix(String[][] arr) {
    for (int i = 0; i < arr.length + 1; i++) {
      System.out.printf(" %d |", i);
    }
    System.out.println();
    for (int i = 0; i < arr.length; i++) {
      System.out.printf(" %d |", i + 1);
      for (int j = 0; j < arr[i].length; j++) {
        System.out.printf(" %s |", arr[i][j]);
      }
      System.out.println();
    }
  }

  public static int scanner() {
    Scanner scanner = new Scanner(System.in);
    if (scanner.hasNextInt()) {
      return scanner.nextInt();
    } else {
      System.out.println("Not a number");
      return scanner();
    }
  }

  public static int getMatrixSize() {
    System.out.println("Choose the size of the matrix. Please enter a number of at least 5.");
    return scanner();
  }

  public static int[] randomTarget() {
    Random random = new Random();
    int[] arr = new int[2];
    arr[0] = random.nextInt(size - 1) + 1;
    arr[1] = random.nextInt(size - 1) + 1;
    System.out.println("The target is: " + arr[0] + ", " + arr[1]);
    return arr;
  }

  public static int[] takeShot() {
    int[] arr = new int[2];
    System.out.println("All set. Get ready to rumble!");
    System.out.printf("Choose a row. Enter a number between 1 and %d.", size);
    arr[0] = scanner() - 1;
    System.out.printf("Choose a column. Enter a number between 1 and %d.", size);
    arr[1] = scanner() - 1;
    return arr;
  }

}
